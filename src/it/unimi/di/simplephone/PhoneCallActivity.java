package it.unimi.di.simplephone;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PhoneCallActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.phone_call);
	}
	
	public void makeCall(View v) {
		EditText editText = (EditText) findViewById(R.id.phone_number);
		String phone = editText.getText().toString();
		String name = "no name";
		
		PhoneCall call = new PhoneCall(this, name, phone);
		call.makeCall();
	}
}
