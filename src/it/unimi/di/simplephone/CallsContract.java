package it.unimi.di.simplephone;

import android.provider.BaseColumns;

public class CallsContract {
	
	public CallsContract() {}
	
	public static abstract class CallEntry implements BaseColumns {
		public static final String TABLE_NAME = "calls";
		public static final String COLUMN_NAME_ID = "id_call";
		public static final String COLUMN_NAME_CONTACT_NAME = "contact_name";
		public static final String COLUMN_NAME_PHONE_NUMBER = "phone_number";
	}
}
