package it.unimi.di.simplephone;

import it.unimi.di.simplephone.CallsContract.CallEntry;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class PhoneCall {
	private Context activityContext;
	private String name, number;
	
	public PhoneCall(Context activity, String name, String number) {
		activityContext = activity;
		this.name = name;
		this.number = number;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void makeCall() {
		saveCall();
		
		Intent i = new Intent(Intent.ACTION_CALL);
		i.setData(Uri.parse("tel:" + number));
		activityContext.startActivity(i);
	}
	
	private void saveCall() {
		CallsDbHelper dbHelper = new CallsDbHelper(activityContext);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(CallEntry.COLUMN_NAME_CONTACT_NAME, name);
		values.put(CallEntry.COLUMN_NAME_PHONE_NUMBER, number);
		
		db.insert(CallEntry.TABLE_NAME, null, values);
		db.close();
	}
	
}
