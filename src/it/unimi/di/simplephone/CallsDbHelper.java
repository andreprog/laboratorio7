package it.unimi.di.simplephone;

import it.unimi.di.simplephone.CallsContract.CallEntry;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CallsDbHelper extends SQLiteOpenHelper {
	private static int DB_VERSION = 1;
	public static final String DATABASE_NAME = "calls.db";
	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + CallEntry.TABLE_NAME + " (" +
		CallEntry._ID + " INTEGER PRIMARY KEY," +
		CallEntry.COLUMN_NAME_ID + " TEXT," +
		CallEntry.COLUMN_NAME_CONTACT_NAME + " TEXT," +
		CallEntry.COLUMN_NAME_PHONE_NUMBER + " TEXT" +
		")";
	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + CallEntry.TABLE_NAME;
	
	public CallsDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ENTRIES);
		onCreate(db);
	}
}
