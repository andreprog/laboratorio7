package it.unimi.di.simplephone;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class MenuDialog extends DialogFragment {
	private final String[] options = {"Chiamata", "SMS", "Mail"};
	private PhoneCall call;
	
	public MenuDialog(PhoneCall call) {
		this.call = call;
	}
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	switch(which) {
            		case 0:
            			call.makeCall();
        				break;
            		case 1:
            			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            			smsIntent.setType("vnd.android-dir/mms-sms");
    					smsIntent.putExtra("address", call.getNumber());
    					smsIntent.putExtra("sms_body", "");
    					startActivity(smsIntent);
    					break;
            		case 2:
            			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            			emailIntent.setType("plain/text");
    					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { "email@example.com" });
    					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, call.getName());
    					emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
    					startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    					break;
            	}
        }
        });
		
		return builder.create();
	}
}