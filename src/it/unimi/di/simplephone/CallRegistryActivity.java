package it.unimi.di.simplephone;

import it.unimi.di.simplephone.CallsContract.CallEntry;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.ListAdapter;

public class CallRegistryActivity extends ListActivity {

	private ListAdapter mAdapter = null;
	
	@Override
	protected void onResume() {
		super.onResume();

		CallsDbHelper dbHelper = new CallsDbHelper(this);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String[] projection = {CallEntry._ID, CallEntry.COLUMN_NAME_CONTACT_NAME, CallEntry.COLUMN_NAME_PHONE_NUMBER};
		
		Cursor c = db.query(CallEntry.TABLE_NAME, projection, null, null, null, null, CallEntry._ID + " DESC", "10");
		
		String columns[] = new String[] {CallEntry.COLUMN_NAME_CONTACT_NAME, CallEntry.COLUMN_NAME_PHONE_NUMBER};
		int names[] = new int[] {R.id.list_names, R.id.list_numbers};
		
		mAdapter = new SimpleCursorAdapter(this, R.layout.row_layout, c, columns, names, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		setListAdapter(mAdapter);
	}
}
