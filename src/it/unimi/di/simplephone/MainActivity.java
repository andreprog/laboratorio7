package it.unimi.di.simplephone;

import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {
	private ListAdapter mAdapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Cursor c = getContentResolver().query(CommonDataKinds.Phone.CONTENT_URI, null, null, null, CommonDataKinds.Phone.DISPLAY_NAME);
		String columns[] = new String[] {CommonDataKinds.Phone.DISPLAY_NAME, CommonDataKinds.Phone.NUMBER};
		int names[] = new int[] {R.id.list_names, R.id.list_numbers};
		
		mAdapter = new SimpleCursorAdapter(this, R.layout.row_layout, c, columns, names, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		
		setListAdapter(mAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Cursor c = (Cursor) mAdapter.getItem(position);
		String name = c.getString(c.getColumnIndex(CommonDataKinds.Phone.DISPLAY_NAME));
		String phone = c.getString(c.getColumnIndex(CommonDataKinds.Phone.NUMBER));
		
		PhoneCall call = new PhoneCall(this, name, phone);
		DialogFragment menu = new MenuDialog(call);
		menu.show(getFragmentManager(), "menu");
	}
	
	public void displayRegistry(View v) {
		Intent intent = new Intent(this, CallRegistryActivity.class);
		startActivity(intent);
	}
	
	public void displayPhoneCall(View v) {
		Intent intent = new Intent(this, PhoneCallActivity.class);
		startActivity(intent);
	}
}
